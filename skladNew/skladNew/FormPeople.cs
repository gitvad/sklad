﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace skladNew
{
    public partial class FormPeople : Form
    {
        public FormPeople()
        {
            InitializeComponent();
        }

        private void FormPeople_Load(object sender, EventArgs e)
        {
            using (skladEntities db = new skladEntities())
            {
                List<Person> persons = db.Persons.ToList();
                listBox1.DataSource = persons;
                listBox1.ValueMember = "id";
                listBox1.DisplayMember = "name";
                //if (hd.Person != null)
                //    fhw.comboBoxPeaple.SelectedValue = hd.Person.id;
            }
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            AddString ap = new AddString();

            DialogResult result = ap.ShowDialog(this);
            if (result == DialogResult.Cancel)
                return;

            using (skladEntities db = new skladEntities())
            {
                //Проверяем на существование объекта
                if (db.Hardwares.Any(o => o.sn == ap.textBox1.Text))
                {
                    MessageBox.Show("Такой человек есть!");
                    return;
                }

                Person user = new Person();
                user.name = ap.textBox1.Text;
                db.Persons.Add(user);
                db.SaveChanges();
                listBox1.DataSource = db.Persons.ToList();
            }

            MessageBox.Show("Ответсвенный добавлен");
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            using (skladEntities db = new skladEntities())
            {
                if ((int)listBox1.SelectedValue != 0)
                {

                    Person st = db.Persons.Find(listBox1.SelectedValue);
                    db.Persons.Remove(st);
                    db.SaveChanges();
                    listBox1.DataSource = db.Persons.ToList();
                    MessageBox.Show("Ответсвенный удален");
                }
                else { MessageBox.Show("Объект не выбран для удаления"); }


            }
        }

        private void buttonChange_Click(object sender, EventArgs e)
        {
            using (skladEntities db = new skladEntities())
            {


                AddString ap = new AddString();
                Person st = db.Persons.Find(listBox1.SelectedValue);

                ap.textBox1.Text = st.name;
                DialogResult result = ap.ShowDialog(this);
                if (result == DialogResult.Cancel)
                    return;

                //Проверяем на существование объекта
                if (db.Persons.Any(o => o.name == ap.textBox1.Text))
                {
                    MessageBox.Show("Данный объект существует");
                    
                    
                    return;
                }


                st.name = ap.textBox1.Text;
                db.Persons.Add(st);
                db.Entry(st).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                MessageBox.Show("Имя ответсвенного измененно");
                listBox1.DataSource = db.Persons.ToList();


            }
        }
    }
}
