//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан по шаблону.
//
//     Изменения, вносимые в этот файл вручную, могут привести к непредвиденной работе приложения.
//     Изменения, вносимые в этот файл вручную, будут перезаписаны при повторном создании кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace skladNew
{
    using System;
    using System.Collections.Generic;
    
    public partial class Hardware
    {
        public int id { get; set; }
        public string name { get; set; }
        public Nullable<int> personsid { get; set; }
        public Nullable<int> rfidsid { get; set; }
        public string sn { get; set; }
        public string text { get; set; }
        public string RFID { get; set; }
        public Nullable<int> statusid { get; set; }
    
        public virtual Person Person { get; set; }
        public virtual rfid rfid { get; set; }
        public virtual Status Status { get; set; }
    }
}
