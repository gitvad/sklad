﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace skladNew
{
    public partial class FormSklad : Form
    {
        public int idsklad2;
        public FormSklad(int idsklad)
        {
            idsklad2 = idsklad;
            InitializeComponent();
        }

        private void buttonAnts_Click(object sender, EventArgs e)
        {
            
            TargetAnts ta = new TargetAnts(idsklad2);
            ta.ShowDialog();
        }
    }
}
