﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace skladNew
{
    public partial class Form1 : Form
    {
        bool isloaded;
        public int idsklad;
        public Form1()
        {
            InitializeComponent();
            
        }

        //Тип данных для истории перемещения
        public class HistoryTable
        {
            public string name { get; set; }
            public DateTime LastDateTime { get; set; }
        }
        //Первична загрузка данных на форму
        private void Form1_Load(object sender, EventArgs e)
        {
            //Загрузка данных
            using (skladEntities db = new skladEntities())

            {
                
                listBox1.DataSource = db.storagerooms.ToList();               
                listBox1.ValueMember = "id";
                listBox1.DisplayMember = "name";               
                isloaded = true;
                if (listBox1.Items.Count > 0) listBox1.SetSelected(0, true);


                //Настройка отображения по умолчанию
                if (dataGridView1.RowCount > 0) dataGridView1.CurrentCell = dataGridView1.Rows[0].Cells[1];
                this.dataGridView1.Columns["hId"].Visible = false;
                this.dataGridView1.Columns["SId"].Visible = false;
                this.dataGridView1.Columns["LastTime"].Visible = true;
                this.dataGridView1.Columns["RFID"].Visible = true;
                this.dataGridView1.RowHeadersVisible = false;




                // if listb.Items.Count > 0 then listBox1.Selected[0]:= True;


                //var result = from h in db.Hardwares
                //             select new { Оборудование = h.name,  RFID = h.rfid.hex };
                //dataGridView1.DataSource = result.ToList();

            }


            //Автоматическое выполнение скрипта в другом потоке
            Thread myThread = new Thread(new ThreadStart(updatelist));
            myThread.Start(); // запускаем поток
            {
                //using (skladEntities db = new skladEntities())
                //{
                //    //var LastHexDateSklad = db.transferhds.GroupBy(c => c.colHexCard)
                //    //                        .Select(g => new
                //    //                        {
                //    //                            name = g.Key,
                //    //                            date = g.Max(x => x.LastDateTime)
                //    //                        })

                //    //    .Join(db.transferhds, p => p.date, d => d.LastDateTime, (p, d) => new // результат
                //    //    {
                //    //        HexName = p.name,
                //    //        Sklad = d.ant.storageroom.name,
                //    //        LastDate = p.date
                //    //    });
                //}
            }
        }

        //При выборе склада
        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            changeSklad();

        }
        //Действия при выборе склада
        private void changeSklad()
        {
            using (skladEntities db = new skladEntities())

            {
                try
                {
                    if (isloaded) //если загружены склады
                    {

                        if (listBox1.SelectedIndex == 0)// если выбрано все оборудование
                        {
                            db.Database.Log = s => System.Diagnostics.Debug.WriteLine(s);




                            var result = from a in db.Hardwares
                                         select new { id = a.id, aa = a.name, aa2 = a.RFID } into ccc
                                         join cc in new
                                         {
                                             result1 = from transferhd1 in db.transferhds
                                                       group transferhd1 by transferhd1.colHexCard into dd
                                                       let MaxOrderDatePerPerson = dd.Max(g => g.LastDateTime)
                                                       join aa in db.transferhds on MaxOrderDatePerPerson equals aa.LastDateTime
                                                       select new { aa.ant.storageroomid, aa.colHexCard, aa.ant.storageroom.name, aa.LastDateTime }
                                         }.result1
                                         on ccc.aa2 equals cc.colHexCard into vv
                                         from vv1 in vv.DefaultIfEmpty()
                                         
                                         select new { hId = ccc.id, Оборудование = ccc.aa, Sid = vv1.storageroomid, Склад = vv1.name, LastTime = vv1.LastDateTime, RFID = ccc.aa2 };

                            //var result = from a in db.Hardwares
                            //             select new { id = a.id, aa = a.name, aa1 = (int?)a.rfid.id, aa2 = a.rfid.hex } into ccc
                            //             join cc in new
                            //             {
                            //                 result1 = from transferhd1 in db.transferhds
                            //                           group transferhd1 by transferhd1.colHexCard into dd
                            //                           let MaxOrderDatePerPerson = dd.Max(g => g.LastDateTime)
                            //                           join aa in db.transferhds on MaxOrderDatePerPerson equals aa.LastDateTime
                            //                           select new { aa.ant.storageroomid, aa.colHexCard, aa.ant.storageroom.name, aa.LastDateTime }
                            //             }.result1
                            //             on ccc.aa2 equals cc.colHexCard into vv
                            //             from vv1 in vv.DefaultIfEmpty()
                            //             select new { hId = ccc.id, Оборудование = ccc.aa, Sid = vv1.storageroomid, Склад = vv1.name, LastTime = vv1.LastDateTime, RFID = ccc.aa2 };

                            dataGridView1.DataSource = result.ToList();



                        }
                        else //если выбран склад
                        {
                            var result = from transferhd1 in db.transferhds
                                         group transferhd1 by transferhd1.colHexCard into dd
                                         let MaxOrderDatePerPerson = dd.Max(g => g.LastDateTime)
                                         join aa in db.transferhds on MaxOrderDatePerPerson equals aa.LastDateTime
                                         join cc in db.Hardwares on dd.Key equals cc.RFID
                                         where aa.ant.storageroom.id == (int)listBox1.SelectedValue
                                         select new { hId = cc.id, Оборудование = cc.name, Sid = aa.ant.storageroomid, LastTime = MaxOrderDatePerPerson, RFID = cc.RFID };

                            //var result2 = from transferhd1 in db.transferhds
                            //             group transferhd1 by transferhd1.colHexCard into dd
                            //             let MaxOrderDatePerPerson = dd.Max(g => g.LastDateTime)
                            //             join aa in db.transferhds on MaxOrderDatePerPerson equals aa.LastDateTime
                            //             join cc in db.Hardwares on dd.Key equals cc.rfid.hex
                            //             where aa.ant.storageroom.id == (int)listBox1.SelectedValue
                            //             select new { hId = cc.id, Оборудование = cc.name, Sid = aa.ant.storageroomid, LastTime = MaxOrderDatePerPerson, RFID = cc.rfid.hex };

                            idsklad = (int)listBox1.SelectedValue;


                            dataGridView1.DataSource = result.ToList();

                        }



                    }
                }
                catch (Exception ex)
                { MessageBox.Show(ex.ToString()); }


            }

        }
        
        //Добавление склада
        private void AddToolStripMenuItem1_Click(object sender, EventArgs e) => addsklad();
        //Редактирование склада
        private void ChangeToolStripMenuItem_Click(object sender, EventArgs e) => changesklad();
        //Удаление склада
        private void RemoveToolStripMenuItem_Click(object sender, EventArgs e) => removesklad();
        //Функция добавления склада
        private void addsklad()
        {
            using (skladEntities db = new skladEntities())
            {


                FormSklad fs = new FormSklad(idsklad);
                fs.buttonAnts.Enabled = false;
                fs.label2.Visible = true;
                DialogResult result = fs.ShowDialog(this);
                if (result == DialogResult.Cancel)
                    return;

                //Проверяем на существование объекта
                if (db.storagerooms.Any(o => o.name == fs.textBoxSkladNmae.Text))
                {
                    MessageBox.Show("Данный объект существует");
                    return;
                }

                storageroom st = new storageroom();
                st.name = fs.textBoxSkladNmae.Text;
                db.storagerooms.Add(st);
                db.SaveChanges();
                listBox1.DataSource = db.storagerooms.ToList();
                MessageBox.Show("Склад добавлен");
            }
            
        }
        //Функция изменения склала
        
        private void changesklad()
        {
            using (skladEntities db = new skladEntities())
            {
                //Проверяем на возможность редактирования
                if ((int)listBox1.SelectedValue == 1)
                {
                    MessageBox.Show("Данный объект изменить нельзя");
                    return;
                }
                FormSklad fs = new FormSklad(idsklad);
                storageroom st = db.storagerooms.Find(listBox1.SelectedValue);
                
                fs.textBoxSkladNmae.Text = st.name;
                string name2 = st.name;
                DialogResult result = fs.ShowDialog(this);
                if (result == DialogResult.Cancel)
                    return;

                //Проверяем на существование объекта
                if (fs.textBoxSkladNmae.Text != name2) {
                    if (db.storagerooms.Any(o => o.name == fs.textBoxSkladNmae.Text))
                    {
                        MessageBox.Show("Данный объект существует");
                        return;
                    }
                }
                

                st.name = fs.textBoxSkladNmae.Text;
                db.storagerooms.Add(st);
                db.Entry(st).State = EntityState.Modified;
                db.SaveChanges();
                listBox1.DataSource = db.storagerooms.ToList();
                

            }
           
        }
        //Функция удаления склала
        private void removesklad()
        {
            using (skladEntities db = new skladEntities())
            {
                if ((int)listBox1.SelectedValue != 1)
                {
                    
                    storageroom st = db.storagerooms.Find(listBox1.SelectedValue);
                    db.storagerooms.Remove(st);
                    db.SaveChanges();
                    listBox1.DataSource = db.storagerooms.ToList();
                    MessageBox.Show("Склад удален");
                }
                else { MessageBox.Show("Данный элемент удалить нельзя"); }
            }
           
        }

        //Вызвать свойство склада
        private void propertiesToolStripMenuItem_Click(object sender, EventArgs e) => FormProperties();

        //История перемещения
        private void History()
        {
            using (skladEntities db = new skladEntities())
            {
                try
                {
                    if (dataGridView1.SelectedRows.Count > 0)
                    {

                        int id = 0;
                        bool converted = Int32.TryParse(dataGridView1.CurrentRow.Cells["hId"].Value.ToString(), out id);
                        if (converted == false)
                            return;
                        Hardware hd = db.Hardwares.Find(id);

                        string strSql = "select storagerooms.name as name, t1.LastDateTime as LastDateTime ";
                        strSql += "from(select id, antid, case when antid <> lead(antid, 1, antid + 1) over(order by id) then 'X' end flg, colHexCard, LastDateTime, addres, HexDecWG, Length, LastTime, RepeatCount ";
                        strSql += "from transferhd ) as t1 ";
                        strSql += "inner join ants ";
                        strSql += "on ants.id = t1.antid ";
                        strSql += "inner join storagerooms ";
                        strSql += "on storagerooms.id = ants.storageroomid ";
                        strSql += " where flg is not NULL ";
                        strSql += "and colHexCard = '";
                        strSql += hd.RFID;
                        strSql += "'order by LastDateTime ";

                        // Смотрим историю через sql запрос, так как ef не позволяет написать такой запрос
                        var idAntsHands = db.Database.SqlQuery<HistoryTable>(strSql);


                        History fh = new History();
                        fh.dataGridView1.RowHeadersVisible = false;
                        fh.dataGridView1.DataSource = idAntsHands.ToList();
                        fh.dataGridView1.Columns[0].HeaderText = "Склад";
                        fh.dataGridView1.Columns[1].HeaderText = "Дата и время";


                        DialogResult result = fh.ShowDialog(this);
                        if (result == DialogResult.OK)
                            return;
                    }
                }
                catch (Exception msg)
                {
                    MessageBox.Show("Error: " + msg.Message);

                }
            }

            }

        //Свойство оборудования
        private void FormProperties()
        {

             

            using (skladEntities db = new skladEntities())
            {


                if (dataGridView1.SelectedRows.Count > 0)
                {

                    int id = 0;
                    bool converted = Int32.TryParse(dataGridView1.CurrentRow.Cells["hId"].Value.ToString(), out id);
                    if (converted == false)
                        return;

                    FormHardWare fhw = new FormHardWare();
                    Hardware hd = db.Hardwares.Find(id);


                    fhw.textBoxName.Text = hd.name;
                    fhw.textBoxText.Text = hd.text;
                    fhw.textBoxSerial.Text = hd.sn;
                    fhw.textBoxRFID.Text = hd.RFID;

                    List<Person> persons = db.Persons.ToList();
                    fhw.comboBoxPeaple.DataSource = persons;
                    fhw.comboBoxPeaple.ValueMember = "id";
                    fhw.comboBoxPeaple.DisplayMember = "name";
                    if (hd.Person != null)
                        fhw.comboBoxPeaple.SelectedValue = hd.Person.id;

                    List<Status> stat = db.Status.ToList();
                    fhw.comboBoxStatus.DataSource = stat;
                    fhw.comboBoxStatus.ValueMember = "id";
                    fhw.comboBoxStatus.DisplayMember = "status";
                    if (hd.Status != null)
                        fhw.comboBoxStatus.SelectedValue = hd.Status.id;



                    //Показать к какому складу привязан
                    IQueryable<storageroom> storageroom = db.storagerooms;
                    fhw.comboBoxStorageRoom.DataSource = storageroom.Where(p => p.id > 1).ToList();
                    fhw.comboBoxStorageRoom.ValueMember = "id";
                    fhw.comboBoxStorageRoom.DisplayMember = "name";                    

                    int ids = 0;
                    if (dataGridView1.CurrentRow.Cells["SId"].Value != null)
                    {
                        bool converteds = Int32.TryParse(dataGridView1.CurrentRow.Cells["SId"].Value.ToString(), out ids);
                        if (converteds == false)
                            return;
                    }
                    storageroom stor = db.storagerooms.Find(ids);
                    if (stor != null)
                        fhw.comboBoxStorageRoom.SelectedValue = stor.id;
                    else
                        fhw.comboBoxStorageRoom.SelectedValue = 1;

                    

                    DialogResult result = fhw.ShowDialog(this);
                    if (result == DialogResult.Cancel)
                        return;
                    



                        hd.name = fhw.textBoxName.Text;
                    hd.text = fhw.textBoxText.Text;
                    hd.sn = fhw.textBoxSerial.Text;
                    hd.RFID = fhw.textBoxRFID.Text;
                    hd.Person = (Person)fhw.comboBoxPeaple.SelectedItem;
                    hd.Status = (Status)fhw.comboBoxStatus.SelectedItem;

                    db.Hardwares.Add(hd);
                    db.Entry(hd).State = EntityState.Modified;
                    db.SaveChanges();
                    changeSklad();

                    // Изменяем склад
                    if (stor != fhw.comboBoxStorageRoom.SelectedItem)
                    {
                        
                        int idantshand = 0;
                        

                        //Изменения в историю перемещения записываю с помощью SQL запроса
                        // если выбранн склад, то полуаю ид первой любой его ручной нтенны
                        if (fhw.comboBoxStorageRoom.SelectedItem != null)
                        {
                            var idAntsHands = db.Database.SqlQuery<ant>("select top 1 id, name, storageroomid from ants where  ants.name like '%Hands%' and ants.storageroomid = " + fhw.comboBoxStorageRoom.SelectedValue.ToString());
                            foreach (var i in idAntsHands)
                                idantshand = i.id;
                        }

                        //Изменения в историю перемещения записываю с помощью SQL запроса
                        // если есть антенна то записываю запись в историю перемещения
                        if (idantshand != 0)
                        {

                            string strSQL = string.Empty;
                            strSQL = "INSERT INTO transferhd ";
                            strSQL += " (antid,   colHexCard, LastTime,  LastDateTime)";
                            strSQL += " VALUES";
                            strSQL += " (@antid,  @colHexCard, @LastTime, @LastDateTime)";

                            List<SqlParameter> parameterList = new List<SqlParameter>();
                            parameterList.Add(new SqlParameter("@antid", idantshand));
                            parameterList.Add(new SqlParameter("@colHexCard", fhw.textBoxRFID.Text));
                            parameterList.Add(new SqlParameter("@LastTime", DateTime.Now));
                            parameterList.Add(new SqlParameter("@LastDateTime", DateTime.Now));
                            SqlParameter[] Param = parameterList.ToArray();
                            int numberOfRowInserted = db.Database.ExecuteSqlCommand(strSQL, Param);
                        }
                        else
                        {
                            MessageBox.Show("Не могу переместить оборудование! Отсутсвует виртуальная аннтена склада для ручного перемещения!");
                        }
                        

                    }

                    

                }

              
            }
            changeSklad();
            //MessageBox.Show("Изменения приняты");
        }
        //Добавить склад
        private void добавитьСкладToolStripMenuItem_Click(object sender, EventArgs e)
        {
            addsklad();
            
        }
        //Редактировать склад
        private void редактироватьСкладToolStripMenuItem_Click(object sender, EventArgs e)
        {
            changesklad();
            
        }
        //Удалить склад
        private void удалитьСкладToolStripMenuItem_Click(object sender, EventArgs e)
        {
            removesklad();
            
        }

       
        //Удалить оборудование
        private void deleteHWToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (skladEntities db = new skladEntities())
            {


                if (dataGridView1.SelectedRows.Count > 0)
                {

                    int id = 0;
                    bool converted = Int32.TryParse(dataGridView1.CurrentRow.Cells["hId"].Value.ToString(), out id);
                    if (converted == false)
                        return;
                    
                    Hardware hd = db.Hardwares.Find(id);
                    db.Hardwares.Remove(hd);
                    db.SaveChanges();
                    changeSklad();

                }

            }
        }
        //Добавить оборудование
        private void AddHWToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (skladEntities db = new skladEntities())
            {

                FormHardWare fhw = new FormHardWare();

                List<Person> persons = db.Persons.ToList();
                fhw.comboBoxPeaple.DataSource = persons;
                fhw.comboBoxPeaple.ValueMember = "id";
                fhw.comboBoxPeaple.DisplayMember = "name";


                List<Status> stat = db.Status.ToList();
                fhw.comboBoxStatus.DataSource = stat;
                fhw.comboBoxStatus.ValueMember = "id";
                fhw.comboBoxStatus.DisplayMember = "status";

                fhw.label6.Visible = false;
                fhw.comboBoxStorageRoom.Visible = false;



                DialogResult result = fhw.ShowDialog(this);
                if (result == DialogResult.Cancel)
                    return;

                //Проверяем на существование объекта
                if (db.Hardwares.Any(o => o.sn == fhw.textBoxSerial.Text))
                {
                    MessageBox.Show("Объект с данным серийным номером существует");
                    return;
                }
                else if (db.Hardwares.Any(o => o.RFID == fhw.textBoxRFID.Text))
                {
                    MessageBox.Show("Объект с данной RFID меткой существует");
                    return;
                }

                Hardware st = new Hardware();
                st.name = fhw.textBoxName.Text;
                st.text = fhw.textBoxText.Text;
                st.sn = fhw.textBoxSerial.Text;
                st.RFID = fhw.textBoxRFID.Text;
                st.Person = (Person)fhw.comboBoxPeaple.SelectedItem;

                db.Hardwares.Add(st);
                db.SaveChanges();
                listBox1.DataSource = db.storagerooms.ToList();
                MessageBox.Show("Оборудование добавлено");

            }
        }
        //Добавить ответсвенного
        private void добавитьОтветсвенногоToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormPeople fp = new FormPeople();
            fp.ShowDialog();
        }

        //Показать историю перемещения
        private void SoreHWToolStripMenuItem_Click(object sender, EventArgs e)
        {
            History();
        }

        //Закрыть программу
        private void выходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //Обновление спика оборудования
        private void updatelist()
        {

            using (skladEntities db = new skladEntities())
            {
                try
                {
                    //выполняется постоянно
                    int aaa = 0;
                    while (true)
                    {
                       //выполняем когда открыто только главное окно
                        int CountForm = Application.OpenForms.Count;
                        if (CountForm == 1)
                        {
                            //выполняем если есть изменения в таблице
                            if (aaa < db.transferhds.Count())
                            {
                                if (this.InvokeRequired)
                                    BeginInvoke(new MethodInvoker(delegate
                                    {
                                        changeSklad();
                                        
                            }));
                            aaa = db.transferhds.Count();
                            }
                            
                        }
                        //если теряем главное окно, то закрываем выполнение
                        if (CountForm == 0)
                        { break; }
                        Thread.Sleep(2000);
                    }
                    
                    

                }
                catch (Exception msg)
                {
                    MessageBox.Show(msg.Message);
                }
            }
        }

       
    }
}
