﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace skladNew
{
    public partial class History : Form
    {
        public History()
        {
            InitializeComponent();
        }

        private void History_Load(object sender, EventArgs e)
        {
            using (skladEntities db = new skladEntities())
            {

                if (dataGridView1.SelectedRows.Count > 0)
                {

                    int id = 0;
                    bool converted = Int32.TryParse(dataGridView1.CurrentRow.Cells["hId"].Value.ToString(), out id);
                    if (converted == false)
                        return;
                    Hardware hd = db.Hardwares.Find(id);



                    string strSql = "select id, antid, addres, HexDecWG, Length, colHexCard, LastTime, RepeatCount, LastDateTime ";
                    strSql += "from (";
                    strSql += "select id, antid, case when antid <> lead(antid, 1, antid + 1) over(order by id) then 'X' end flg, colHexCard, LastDateTime, addres, HexDecWG, Length, LastTime, RepeatCount ";
                    strSql += "from transferhd) as t1 ";
                    strSql += "where flg is not NULL ";
                    strSql += "and colHexCard = '";
                    strSql += hd.RFID;
                    strSql += "' order by id";


                    //var idAntsHands = db.Database.SqlQuery<transferhd>(strSql);
                    var historyHW = from t in db.Database.SqlQuery<transferhd>(strSql)
                                    select new { id = t.id, stor = t.ant.storageroom.name, time = t.LastDateTime };


                    History fh = new History();

                    fh.dataGridView1.DataSource = historyHW.ToList();
                    fh.dataGridView1.Columns["id"].Visible = true;
                    DialogResult result = fh.ShowDialog(this);
                    if (result == DialogResult.OK)
                        return;
                }
            }
        }
    }
}
