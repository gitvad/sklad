﻿namespace skladNew
{
    partial class FormSklad
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxSkladNmae = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonAnts = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // textBoxSkladNmae
            // 
            this.textBoxSkladNmae.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.textBoxSkladNmae.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.textBoxSkladNmae.Location = new System.Drawing.Point(16, 36);
            this.textBoxSkladNmae.Name = "textBoxSkladNmae";
            this.textBoxSkladNmae.Size = new System.Drawing.Size(227, 20);
            this.textBoxSkladNmae.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.button1.Location = new System.Drawing.Point(16, 164);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "&OK";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button2.Location = new System.Drawing.Point(168, 164);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 2;
            this.button2.Text = "&Отмена";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(99, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Название склада:";
            // 
            // buttonAnts
            // 
            this.buttonAnts.Location = new System.Drawing.Point(16, 72);
            this.buttonAnts.Name = "buttonAnts";
            this.buttonAnts.Size = new System.Drawing.Size(227, 23);
            this.buttonAnts.TabIndex = 4;
            this.buttonAnts.Text = "Привязать антенну";
            this.buttonAnts.UseVisualStyleBackColor = true;
            this.buttonAnts.Click += new System.EventHandler(this.buttonAnts_Click);
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(13, 113);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(230, 48);
            this.label2.TabIndex = 5;
            this.label2.Text = "Внимание!!! Привязать антенну возможно только после добавления склада!";
            this.label2.Visible = false;
            // 
            // FormSklad
            // 
            this.AcceptButton = this.button1;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.button2;
            this.ClientSize = new System.Drawing.Size(260, 208);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.buttonAnts);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBoxSkladNmae);
            this.Name = "FormSklad";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        protected internal System.Windows.Forms.TextBox textBoxSkladNmae;
        private System.Windows.Forms.Label label1;
        protected internal System.Windows.Forms.Button buttonAnts;
        protected internal System.Windows.Forms.Label label2;
    }
}