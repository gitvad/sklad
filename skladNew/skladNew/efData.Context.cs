﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан по шаблону.
//
//     Изменения, вносимые в этот файл вручную, могут привести к непредвиденной работе приложения.
//     Изменения, вносимые в этот файл вручную, будут перезаписаны при повторном создании кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace skladNew
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class skladEntities : DbContext
    {
        public skladEntities()
            : base("name=skladEntities")
        {
            Database.SetInitializer<skladEntities>(new MyDbInitializer());
        }

        public class MyDbInitializer : CreateDatabaseIfNotExists<skladEntities>
        {
            protected override void Seed(skladEntities context)
            {
                var vse = new storageroom  { id =1, name = "Всё оборудование"};
                var sklad1 = new storageroom { id = 2, name = "Склад1" };
                var sklad2 = new storageroom { id = 3, name = "Склад2" };
                var sklad3 = new storageroom { id = 4, name = "Склад3" };
                var sklad4 = new storageroom { id = 5, name = "Склад4" };
                context.storagerooms.AddRange(new List<storageroom> { vse, sklad1, sklad2, sklad3, sklad4 });
               
                var an1 = new ant { id = 1, name = "Антенна 1" };
                var an2 = new ant { id = 2, name = "Антенна 2" };
                var an3 = new ant { id = 3, name = "Антенна 3" };
                var an4 = new ant { id = 4, name = "Антенна 4" };
                var an5 = new ant { id = 5, name = "АнтеннаHands 1" };
                var an6 = new ant { id = 6, name = "АнтеннаHands 2" };
                var an7 = new ant { id = 7, name = "АнтеннаHands 3" };
                var an8 = new ant { id = 8, name = "АнтеннаHands 4" };
                context.ants.AddRange(new List<ant> {an1, an2, an3,an4, an5, an6, an7, an8});
               
                var s1 = new Status { id = 1, status = "Работает" };
                var s2 = new Status { id = 2, status = "Не Работает" };
                context.Status.Add(s1);
                context.Status.Add(s2);

                context.SaveChanges();
            }
        }

        

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<ant> ants { get; set; }
        public virtual DbSet<Hardware> Hardwares { get; set; }
        public virtual DbSet<Person> Persons { get; set; }
        public virtual DbSet<rfid> rfids { get; set; }
        public virtual DbSet<Status> Status { get; set; }
        public virtual DbSet<storageroom> storagerooms { get; set; }
        public virtual DbSet<sysdiagram> sysdiagrams { get; set; }
        public virtual DbSet<transferhd> transferhds { get; set; }
    }
}
