﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace skladNew
{
    public partial class TargetAnts : Form
    {
        public int idsklad3;
        public TargetAnts(int idsklad)
        {
            idsklad3 = idsklad;
            InitializeComponent();
          
        }

        private void TargetAnts_Load(object sender, EventArgs e)
        {
            using (skladEntities db = new skladEntities())
            {
                IQueryable<ant> an = db.ants;
                listBox1.DataSource = an.Where(p => p.storageroomid == idsklad3).ToList();
                listBox1.ValueMember = "id";
                listBox1.DisplayMember = "name";

                listBox2.DataSource = an.Where(p => p.storageroomid == null ).ToList();
                listBox2.ValueMember = "id";
                listBox2.DisplayMember = "name";
                
                
            }
            }

        //Удалить антену
        private void button1_Click(object sender, EventArgs e)
        {
            using (skladEntities db = new skladEntities())
            {
                ant st = db.ants.Find(listBox1.SelectedValue);
                st.storageroomid = null;
                db.SaveChanges();
                IQueryable<ant> an = db.ants;
                listBox1.DataSource = an.Where(p => p.storageroomid == idsklad3).ToList();
                listBox2.DataSource = an.Where(p => p.storageroomid == null).ToList();


            }
        }
        
        //Назначить антену
        private void button2_Click(object sender, EventArgs e)
        {
            using (skladEntities db = new skladEntities())
            {
                ant st = db.ants.Find(listBox2.SelectedValue);

                storageroom stor = db.storagerooms.Find(idsklad3);
                st.storageroom = stor;
                db.ants.Add(st);
                db.Entry(st).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                IQueryable<ant> an = db.ants;
                listBox1.DataSource = an.Where(p => p.storageroomid == idsklad3).ToList();
                listBox2.DataSource = an.Where(p => p.storageroomid == null).ToList();



            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
