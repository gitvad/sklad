﻿namespace Aosid
{
    partial class frmDBConfig
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.NameServerBD = new System.Windows.Forms.Label();
            this.textBoxServerName = new System.Windows.Forms.TextBox();
            this.UserBD = new System.Windows.Forms.Label();
            this.PassBD = new System.Windows.Forms.Label();
            this.NameBD = new System.Windows.Forms.Label();
            this.textBoxBDName = new System.Windows.Forms.TextBox();
            this.textBoxDBUser = new System.Windows.Forms.TextBox();
            this.textBoxPassDB = new System.Windows.Forms.TextBox();
            this.CheckBD = new System.Windows.Forms.Button();
            this.DBCancel = new System.Windows.Forms.Button();
            this.DBOK = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // NameServerBD
            // 
            this.NameServerBD.AutoSize = true;
            this.NameServerBD.Location = new System.Drawing.Point(27, 48);
            this.NameServerBD.Name = "NameServerBD";
            this.NameServerBD.Size = new System.Drawing.Size(77, 13);
            this.NameServerBD.TabIndex = 0;
            this.NameServerBD.Text = "Имя сервера:";
            // 
            // textBoxServerName
            // 
            this.textBoxServerName.Location = new System.Drawing.Point(134, 45);
            this.textBoxServerName.Name = "textBoxServerName";
            this.textBoxServerName.Size = new System.Drawing.Size(205, 20);
            this.textBoxServerName.TabIndex = 1;
            // 
            // UserBD
            // 
            this.UserBD.AutoSize = true;
            this.UserBD.Location = new System.Drawing.Point(27, 100);
            this.UserBD.Name = "UserBD";
            this.UserBD.Size = new System.Drawing.Size(83, 13);
            this.UserBD.TabIndex = 2;
            this.UserBD.Text = "Пользователь:";
            // 
            // PassBD
            // 
            this.PassBD.AutoSize = true;
            this.PassBD.Location = new System.Drawing.Point(27, 124);
            this.PassBD.Name = "PassBD";
            this.PassBD.Size = new System.Drawing.Size(48, 13);
            this.PassBD.TabIndex = 2;
            this.PassBD.Text = "Пароль:";
            // 
            // NameBD
            // 
            this.NameBD.AutoSize = true;
            this.NameBD.Location = new System.Drawing.Point(27, 74);
            this.NameBD.Name = "NameBD";
            this.NameBD.Size = new System.Drawing.Size(101, 13);
            this.NameBD.TabIndex = 3;
            this.NameBD.Text = "Имя базы данных:";
            // 
            // textBoxBDName
            // 
            this.textBoxBDName.Location = new System.Drawing.Point(134, 71);
            this.textBoxBDName.Name = "textBoxBDName";
            this.textBoxBDName.Size = new System.Drawing.Size(205, 20);
            this.textBoxBDName.TabIndex = 1;
            // 
            // textBoxDBUser
            // 
            this.textBoxDBUser.Location = new System.Drawing.Point(134, 97);
            this.textBoxDBUser.Name = "textBoxDBUser";
            this.textBoxDBUser.Size = new System.Drawing.Size(205, 20);
            this.textBoxDBUser.TabIndex = 1;
            // 
            // textBoxPassDB
            // 
            this.textBoxPassDB.Location = new System.Drawing.Point(134, 121);
            this.textBoxPassDB.Name = "textBoxPassDB";
            this.textBoxPassDB.PasswordChar = '*';
            this.textBoxPassDB.Size = new System.Drawing.Size(205, 20);
            this.textBoxPassDB.TabIndex = 1;
            // 
            // CheckBD
            // 
            this.CheckBD.Location = new System.Drawing.Point(134, 157);
            this.CheckBD.Name = "CheckBD";
            this.CheckBD.Size = new System.Drawing.Size(205, 23);
            this.CheckBD.TabIndex = 4;
            this.CheckBD.Text = "Проверить подключение к БД";
            this.CheckBD.UseVisualStyleBackColor = true;
            this.CheckBD.Click += new System.EventHandler(this.CheckBD_Click);
            // 
            // DBCancel
            // 
            this.DBCancel.Location = new System.Drawing.Point(238, 202);
            this.DBCancel.Name = "DBCancel";
            this.DBCancel.Size = new System.Drawing.Size(101, 23);
            this.DBCancel.TabIndex = 5;
            this.DBCancel.Text = "Отмена";
            this.DBCancel.UseVisualStyleBackColor = true;
            this.DBCancel.Click += new System.EventHandler(this.DBCancel_Click);
            // 
            // DBOK
            // 
            this.DBOK.Location = new System.Drawing.Point(134, 202);
            this.DBOK.Name = "DBOK";
            this.DBOK.Size = new System.Drawing.Size(101, 23);
            this.DBOK.TabIndex = 5;
            this.DBOK.Text = "OK";
            this.DBOK.UseVisualStyleBackColor = true;
            this.DBOK.Click += new System.EventHandler(this.DBOK_Click);
            // 
            // frmDBConfig
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.ClientSize = new System.Drawing.Size(401, 270);
            this.Controls.Add(this.DBOK);
            this.Controls.Add(this.DBCancel);
            this.Controls.Add(this.CheckBD);
            this.Controls.Add(this.NameBD);
            this.Controls.Add(this.PassBD);
            this.Controls.Add(this.UserBD);
            this.Controls.Add(this.textBoxPassDB);
            this.Controls.Add(this.textBoxDBUser);
            this.Controls.Add(this.textBoxBDName);
            this.Controls.Add(this.textBoxServerName);
            this.Controls.Add(this.NameServerBD);
            this.Name = "frmDBConfig";
            this.Text = "Настройка подключение к БД";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label NameServerBD;
        private System.Windows.Forms.TextBox textBoxServerName;
        private System.Windows.Forms.Label UserBD;
        private System.Windows.Forms.Label PassBD;
        private System.Windows.Forms.Label NameBD;
        private System.Windows.Forms.TextBox textBoxBDName;
        private System.Windows.Forms.TextBox textBoxDBUser;
        private System.Windows.Forms.TextBox textBoxPassDB;
        private System.Windows.Forms.Button CheckBD;
        private System.Windows.Forms.Button DBCancel;
        private System.Windows.Forms.Button DBOK;
    }
}