﻿namespace Aosid
{
    partial class Authorization
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.AuthUser = new System.Windows.Forms.Label();
            this.AuthPass = new System.Windows.Forms.Label();
            this.textBoxAuthUser = new System.Windows.Forms.TextBox();
            this.textBoxAuthPass = new System.Windows.Forms.TextBox();
            this.buttonAuthOk = new System.Windows.Forms.Button();
            this.buttonAuthCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // AuthUser
            // 
            this.AuthUser.AutoSize = true;
            this.AuthUser.Location = new System.Drawing.Point(33, 45);
            this.AuthUser.Name = "AuthUser";
            this.AuthUser.Size = new System.Drawing.Size(83, 13);
            this.AuthUser.TabIndex = 0;
            this.AuthUser.Text = "Пользователь:";
            // 
            // AuthPass
            // 
            this.AuthPass.AutoSize = true;
            this.AuthPass.Location = new System.Drawing.Point(68, 81);
            this.AuthPass.Name = "AuthPass";
            this.AuthPass.Size = new System.Drawing.Size(48, 13);
            this.AuthPass.TabIndex = 0;
            this.AuthPass.Text = "Пароль:";
            // 
            // textBoxAuthUser
            // 
            this.textBoxAuthUser.Location = new System.Drawing.Point(122, 42);
            this.textBoxAuthUser.Name = "textBoxAuthUser";
            this.textBoxAuthUser.Size = new System.Drawing.Size(145, 20);
            this.textBoxAuthUser.TabIndex = 1;
            // 
            // textBoxAuthPass
            // 
            this.textBoxAuthPass.Location = new System.Drawing.Point(122, 78);
            this.textBoxAuthPass.MaxLength = 8;
            this.textBoxAuthPass.Name = "textBoxAuthPass";
            this.textBoxAuthPass.PasswordChar = '*';
            this.textBoxAuthPass.Size = new System.Drawing.Size(145, 20);
            this.textBoxAuthPass.TabIndex = 1;
            // 
            // buttonAuthOk
            // 
            this.buttonAuthOk.Location = new System.Drawing.Point(111, 127);
            this.buttonAuthOk.Name = "buttonAuthOk";
            this.buttonAuthOk.Size = new System.Drawing.Size(75, 23);
            this.buttonAuthOk.TabIndex = 2;
            this.buttonAuthOk.Text = "OK";
            this.buttonAuthOk.UseVisualStyleBackColor = true;
            this.buttonAuthOk.Click += new System.EventHandler(this.buttonAuthOk_Click);
            // 
            // buttonAuthCancel
            // 
            this.buttonAuthCancel.Location = new System.Drawing.Point(192, 127);
            this.buttonAuthCancel.Name = "buttonAuthCancel";
            this.buttonAuthCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonAuthCancel.TabIndex = 2;
            this.buttonAuthCancel.Text = "Отмена";
            this.buttonAuthCancel.UseVisualStyleBackColor = true;
            this.buttonAuthCancel.Click += new System.EventHandler(this.buttonAuthCancel_Click);
            // 
            // Authorization
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.ClientSize = new System.Drawing.Size(372, 200);
            this.ControlBox = false;
            this.Controls.Add(this.buttonAuthCancel);
            this.Controls.Add(this.buttonAuthOk);
            this.Controls.Add(this.textBoxAuthPass);
            this.Controls.Add(this.textBoxAuthUser);
            this.Controls.Add(this.AuthPass);
            this.Controls.Add(this.AuthUser);
            this.Name = "Authorization";
            this.Text = "Authorization";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label AuthUser;
        private System.Windows.Forms.Label AuthPass;
        private System.Windows.Forms.TextBox textBoxAuthUser;
        private System.Windows.Forms.TextBox textBoxAuthPass;
        private System.Windows.Forms.Button buttonAuthOk;
        private System.Windows.Forms.Button buttonAuthCancel;
    }
}