﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;

namespace Aosid
{
    class DBUtils
    {
        public static SqlConnection GetDBConnection()
        {
            string datasource = Properties.Settings.Default.ServerDB;

            string database = Properties.Settings.Default.NameDB;
            string username = Properties.Settings.Default.UserDB;
            string password = Properties.Settings.Default.PassDB;

            return DBSQLServerUtils.GetDBConnection(datasource, database, username, password);
        }
    }
}
