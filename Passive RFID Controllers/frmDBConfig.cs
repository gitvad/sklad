﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Aosid
{
    public partial class frmDBConfig : Form
    {
        public frmDBConfig()
        {
            InitializeComponent();
            textBoxServerName.Text = Properties.Settings.Default.ServerDB;
            textBoxBDName.Text = Properties.Settings.Default.NameDB;
            textBoxDBUser.Text = Properties.Settings.Default.UserDB;
            textBoxPassDB.Text = Properties.Settings.Default.PassDB;

        }
        

        private void CheckBD_Click(object sender, EventArgs e)
        {
            try
            {
                
                SqlConnection conntest = DBSQLServerUtils.GetDBConnection(textBoxServerName.Text, textBoxBDName.Text, textBoxDBUser.Text, textBoxPassDB.Text);
                conntest.Open();
                Log.WriteString("Проверка БД Выполнена Успешно");
                MessageBox.Show("Connection successful!");
                conntest.Close();

            }
            catch (Exception ee)
            {
                Log.Write(ee);
                MessageBox.Show("Error: " + ee.Message);
              
            }

        }

        private void DBOK_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.ServerDB= textBoxServerName.Text;
            Properties.Settings.Default.NameDB= textBoxBDName.Text;
            Properties.Settings.Default.UserDB=textBoxDBUser.Text;
            Properties.Settings.Default.PassDB=textBoxPassDB.Text;
            Properties.Settings.Default.Save();
            this.Close();
        }

        private void DBCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
